package com.emedinaa.kotlinapp

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.core.graphics.component1
import androidx.core.graphics.drawable.toDrawable
import androidx.core.graphics.toColor
import androidx.core.graphics.toColorLong
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_web.*
import kotlin.concurrent.fixedRateTimer

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val FRAGMENT_WEB = 0
    private val FRAGMENT_SHOPPING = 1
    private val FRAGMENT_VIDEOS = 2

    private val indicatorButtons = mutableListOf<Button>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        indicatorButtons.add(buttonWeb)
        indicatorButtons.add(buttonShopping)
        indicatorButtons.add(buttonVideos)
        indicatorButtons.forEach {
            it.setOnClickListener(this)
        }
        selectFirst()
    }

    private fun selectFirst() {
        val bundle = Bundle()
        val fragmentId = FRAGMENT_WEB
        updateUI(fragmentId)
        changeFragment(fragmentId)
    }

    override fun onClick(v: View?) {
        val bundle = Bundle()
        var fragmentId = when (v?.id) {
            R.id.buttonWeb -> FRAGMENT_WEB
            R.id.buttonVideos -> FRAGMENT_VIDEOS
            R.id.buttonShopping -> FRAGMENT_SHOPPING
            else -> FRAGMENT_SHOPPING
        }
        changeFragment(fragmentId)
        updateUI(fragmentId)
    }

    private fun updateUI(fragmentId: Int) {
        var position = 0
        indicatorButtons.forEach {
            indicatorButtons[position].setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                null,
                ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
            )
            position++
        }
        indicatorButtons[fragmentId].setCompoundDrawablesWithIntrinsicBounds(
            null,
            null,
            null,
            ContextCompat.getDrawable(this, R.drawable.shape_line)
        )
    }

    private fun changeFragment(fragmentId: Int) {
        val fragment = factoryFragment(fragmentId)
        fragment?.let {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fLayoutContainer, it)
                commit()
            }
        }
    }

    private fun factoryFragment(fragmentId: Int): Fragment? {
        when (fragmentId) {
            FRAGMENT_WEB -> return WebFragment()
            FRAGMENT_SHOPPING -> return ShoppingFragment()
            FRAGMENT_VIDEOS -> return VideosFragment()
        }
        return null
    }

}


